# Composer
Composer program is dependency management tool for PHP. It defines the libraries your project needs and installs and updates the related packages for you.<small><i>Composer programı PHP için bağımlılık yönetim aracıdır. Projenizin ihtiyaç duyduğu kütüphaneleri tanımlar ve sizin için ilgili paketlerin kurulmasını ve güncellemesini yapar.</small></i>
> _Emrullah İLÇİN_
  
## Install
Composer software is installed by curl, php or download. <small><i>Composer yazılımı curl, php veya indirme ile kurulur.</small></i>

> If curl is not installed, install `sudo apt install curl`

### `Local Installation:`
It is recommended that users install the "Composer" program in their own (home) local directory. <small><i>Kullanıcıların "Composer" programını kendi (ev) lokal dizinlerine kurmaları önerilir.</small></i>

* `curl -sS https://getcomposer.org/installer | php -- --install-dir=$HOME --filename=composer` # with curl 
* `php -r "readfile('https://getcomposer.org/installer');" |php -- --install-dir=$HOME --filename=composer` # with php .
* `export PATH=$PATH:/home/emr`  # temporary "path" definition.
* Write `export PATH=$PATH:/home/emr` in `~/.bashrc` with editor (ex: vi) # permanent "path" definition
* see : `echo $PATH`

### `Global Installation:` ( in "/usr/local/bin")
* `sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer` # with curl
* Write `export PATH=$PATH:/home/emr` in `/etc/profile` with editor (ex: vi) # permanent "path" definition
* see : `echo $PATH`
 
## Update: 
`composer update` #  
> If you want to exclude plugins and scripts: Use `composer --no-plugins --no-scripts`

> cache clear `composer clearcache`

`composer global update laravel/installer`  # global update.

> NOT: If there is insufficient memory in the `composer update` process, it may be necessary to stop some programs. ex: apache2, mysql; then run the services again


## Test:
`composer --version`   # see version
`composer show` # see installed packages

## Install and uninstall packages

_ex: xyz/def packages_ 

`composer require xyz/def` # xyz/dev package is installed<br>
`composer remove xyz/def` # xyz/dev package is removed

> :bulb: `php /usr/local/bin/composer dump-autoload`  # composer refresh




